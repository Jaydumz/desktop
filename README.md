How to Use iTunes
iTunes has become far more than a simple music player over the years. 
It is the primary way to manage your iOS device's music and videos, 
it's one of the most popular music stores in the world, and it even lets you burn CDs. 
Getting a handle on its basic features as well as some of its more hidden capabilities 
will help you get most out of iTunes as a media manager and player.

1.Use the buttons along the top to change between music, movies, TV shows, and other files. Beneath the playback controls you'll see several media buttons, including a musical note, a film strip, a TV, and a "..." button. 
Clicking one of these buttons will switch your view to the corresponding "library", or collection of files.
Click the "..." button to see the other libraries that aren't shown by default. You can click "Edit" and check the items that you want to always be displayed.
When you insert a CD or connect your iOS device to your computer, a button for it will appear in this row as well.
You can quickly switch between different libraries by holding Ctrl (Windows) or ⌘ Cmd (Mac) and pressing a number key. For example, Ctrl+1 in Windows will open the Music library.

2.View your playlists by selecting a library and then clicking the "Playlists" tab. 
This will display the media library along with all of your playlists in the sidebar. 
You can drag and drop items to and from playlists using this view.

3.Change the way your current library is displayed by clicking the "View" button in the upper-right corner. 
This will allow you to switch between methods for organizing your media.
For example, if you are in your Music library, the default view is "Albums". 
Click "Albums" to switch to a different sorting method, such as "Songs" or "Artists".

4.Sign in with your Apple ID. Your Apple ID will allow you to sync all of your purchases as well as link your iTunes program to your iOS device. 
If you don't have an Apple ID, you can create one for free.
Click the User button, located to the left of the Search bar.
Sign in with your Apple ID. If you don't have one, click Create Apple ID to make one for free.
If you want to make an Apple ID but don't have a credit card, click here for alternate account creation instructions.

HOW TO INSTALL ITUNES

1.Close all open apps.
2.Open an internet browser then navigate to the iTunes Download web page.
3.Note http://www.apple.com/itunes/download/
4.Click Get it from Microsoft.
5.Click Get.
6.Click Save.
7.Note Note or select the location and name of the file.
8.Click Save.
9.Once the download has completed, click Run.
Note If unavailable, locate then double-click the downloaded file.
10.Click Next.
11.Select the any of the following options then click Install.
Note If prompted to allow changes, click Yes.
12.Add iTunes shortcut to my desktop
13.Use iTunes as the default player for audio files
14.Automatically update iTunes and other Apple software
15.When presented, click Finish.